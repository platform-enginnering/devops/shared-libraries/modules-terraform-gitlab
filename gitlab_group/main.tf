### Modulo Terrafom Gitlab ###
# main.tf #

resource "gitlab_group" "group" {

  description                   = var.description_group
  name                          = var.name_group
  path                          = var.path_group
  parent_id                     = var.parent_id
  share_with_group_lock         = var.share_with_group_lock
  prevent_forking_outside_group = var.prevent_forking_outside_group
}
