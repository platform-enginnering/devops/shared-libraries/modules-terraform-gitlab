### Modulo Terrafom Gitlab ###
# inputs.tf #

variable "repo_id" {
  description = "Id Gitlab Repository File"
  type        = string
}

variable "repo_path" {
  description = "Path Gitlab Repository File"
  type        = string
}

variable "repo_branch" {
  description = "Branch Gitlab Repository File"
  type        = string
}

variable "repo_content" {
  description = "Content Gitlab Repository File"
  type        = string
}

variable "author_email" {
  description = "Author Email Gitlab Repository File"
  type        = string
}

variable "author_name" {
  description = "Author Name Gitlab Repository File"
  type        = string
}

variable "commit_message" {
  description = "Commit Message Gitlab Repository File"
  type        = string
}
