### Modulo Terrafom Gitlab ###
# inputs.tf #


variable "group_id" {
  description = "Group Id Gitlab Project"
  type        = string
}

variable "project_id" {
  description = "Project Id Gitlab Project"
  type        = string
}

variable "group_access" {
  description = "Group Access Gitlab Project Shared Group"
  type        = string
}
